# Exercice 

C'est un exercice pour s'entraîner à `git rebase` 

## Pour aller plus loin : Branches, merges et autres rebases (guidé)

1. Créez une branche **test** sur un projet git :
```
$ git branch test
$ git branch
```

2. Créez un *commit* sur **master** qui modifie le `README.md`.
3. Passez sur la branche **test** :
```
$ git checkout test
```
4. Créez un *commit* qui rajoute un *nouveau* fichier (pour ne pas qu’il y ait de conflits plus tard).
5. Voyez maintenant où vous en êtes avec :
```
$ git log --graph --decorate --oneline --all
```
6. Comme c’est une commande pratique, vous pouvez en faire un alias :
```
$ git config --global alias.lola "log --graph --decorate --oneline --all"
$ git lola
```
7. Il est temps de faire un *merge* pour intégrer la branche **test** dans la branche **master** :
```
$ git checkout master
$ git merge test
$ git lola
```
8. Un *rebase* aurait permit d’avoir un historique linéaire. Annulez d’abord le dernier *commit*, qui
est le **merge** (attention commande dangereuse, vérifiez bien avec `git lola` ou `git log` que vous
vous trouvez bien sur le bon commit !) :
```
$ git reset --hard HEAD~1
```
9. Vous êtes normalement revenu à l’état précédent, comme si le *merge* n’avait pas eu lieu.
10. Faites maintenant le *rebase* :
```
$ git checkout test
$ git rebase master
$ git lola
```
11. La branche **test** se trouve maintenant juste au-dessus de **master**, le **merge** se fera en « *fast-forward* » :
```
$ git checkout master
$ git merge test
```
12. OK toujours vivant ? Compliquons un peu les choses : faites un *commit* sur **master** qui modifie
une certaine ligne d’un fichier. Allez sur la branche **test** et créez un autre *commit* qui modifie
exactement la même ligne. Le *merge* donnera un conflit. En voici un exemple :

|Où/quand       | Affiché       |
|---------------|---------------|
|Avant          | hello world   |
|Branche master | goodbye world |
|Branche test   | Hello, world !|
|Merge Goodbye  |, world !      |

13. Éditez le fichier pour régler le conflit, et suivez les instructions données par la commande *merge*.
Une fois terminé, la branche **test** ne sert plus à rien, vous pouvez la supprimer :
```
$ git branch -d test
```